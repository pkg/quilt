Subject: Avoid warnings with grep 3.8
Origin: v0.67-29-gf73f8d7 <https://git.savannah.nongnu.org/cgit/quilt.git/commit/?id=f73f8d7f71de2878d3f92881a5fcb8eafd78cb5f>
Upstream-Author: Jean Delvare <jdelvare@suse.de>
Date: Fri Sep 9 10:10:37 2022 +0200
Bug-Debian: https://bugs.debian.org/1020108

    GNU grep version 3.8 became more strict about needless quoting in
    patterns. We have one occurrence of that in quilt, where "/"
    characters are being quoted by default. There are cases where they
    indeed need to be quoted (typically when used in a sed s/// command)
    but most of the time they do not, and this results in the following
    warning:

    grep: warning: stray \ before /

    So rename quote_bre() to quote_sed_re(), and introduce
    quote_grep_re() which does not quote "/".

    Signed-off-by: Jean Delvare <jdelvare@suse.de>

--- a/quilt/diff.in
+++ b/quilt/diff.in
@@ -260,7 +260,7 @@
 	# Add all files in the snapshot into the file list (they may all
 	# have changed).
 	files=( $(find $QUILT_PC/$snap_subdir -type f \
-		  | sed -e "s/^$(quote_bre $QUILT_PC/$snap_subdir/)//" \
+		  | sed -e "s/^$(quote_sed_re $QUILT_PC/$snap_subdir/)//" \
 		  | sort) )
 	printf "%s\n" "${files[@]}" >&4
 	unset files
--- a/quilt/patches.in
+++ b/quilt/patches.in
@@ -71,7 +71,7 @@
 	# Quote each file name only once
 	for file in "${opt_files[@]}"
 	do
-		files_bre[${#files_bre[@]}]=$(quote_bre "$file")
+		files_bre[${#files_bre[@]}]=$(quote_grep_re "$file")
 	done
 
 	# "Or" all files in a single pattern
--- a/quilt/scripts/patchfns.in
+++ b/quilt/scripts/patchfns.in
@@ -90,8 +90,14 @@
 	done
 }
 
-# Quote a string for use in a basic regular expression.
-quote_bre()
+# Quote a string for use in a regular expression for a grep pattern.
+quote_grep_re()
+{
+	echo "$1" | sed -e 's:\([][^$.*\\]\):\\\1:g'
+}
+
+# Quote a string for use in a regular expression for a sed s/// command.
+quote_sed_re()
 {
 	echo "$1" | sed -e 's:\([][^$/.*\\]\):\\\1:g'
 }
@@ -227,7 +233,7 @@
 
 	if [ -e "$SERIES" ]
 	then
-		grep -q "^$(quote_bre $patch)\([ \t]\|$\)" "$SERIES"
+		grep -q "^$(quote_grep_re $patch)\([ \t]\|$\)" "$SERIES"
 	else
 		return 1
 	fi
@@ -377,7 +383,7 @@
 {
 	local patch=$1
 	[ -e $DB ] || return 1
-	grep -q "^$(quote_bre $patch)\$" $DB
+	grep -q "^$(quote_grep_re $patch)\$" $DB
 }
 
 applied_patches()
@@ -477,7 +483,7 @@
 	local tmpfile
 	if tmpfile=$(gen_tempfile)
 	then
-		grep -v "^$(quote_bre $patch)\$" $DB > $tmpfile
+		grep -v "^$(quote_grep_re $patch)\$" $DB > $tmpfile
 		cat $tmpfile > $DB
 		rm -f $tmpfile
 		[ -s $DB ] || rm -f $DB
@@ -542,7 +548,7 @@
 		fi
 
 		local patch=${1#$SUBDIR_DOWN$QUILT_PATCHES/}
-		local bre=$(quote_bre "$patch")
+		local bre=$(quote_sed_re "$patch")
 		set -- $(sed -e "/^$bre\(\|\.patch\|\.diff\?\)\(\|\.gz\|\.bz2\|\.xz\|\.lzma\|\.lz\)\([ "$'\t'"]\|$\)/!d" \
 			       -e 's/[ '$'\t''].*//' "$SERIES")
 		if [ $# -eq 1 ]
@@ -653,7 +659,7 @@
 	then
 		find "$path" -type f \
 			       -a ! -path "$(quote_glob "$path")/.timestamp" |
-		sed -e "s/$(quote_bre "$path")\///"
+		sed -e "s/$(quote_sed_re "$path")\///"
 	fi
 }
 
--- a/quilt/upgrade.in
+++ b/quilt/upgrade.in
@@ -76,7 +76,7 @@
 
 for patch in $(applied_patches)
 do
-	proper_name="$(grep "^$(quote_bre $patch)"'\(\|\.patch\|\.diff?\)\(\|\.gz\|\.bz2\)\([ \t]\|$\)' $SERIES)"
+	proper_name="$(grep "^$(quote_grep_re $patch)"'\(\|\.patch\|\.diff?\)\(\|\.gz\|\.bz2\)\([ \t]\|$\)' $SERIES)"
 	proper_name=${proper_name#$QUILT_PATCHES/}
 	proper_name=${proper_name%% *}
 	if [ -z "$proper_name" ]
@@ -86,7 +86,7 @@
 	fi
 
 	if [ "$patch" != "$proper_name" -a -d $QUILT_PC/$patch ] \
-	   && grep -q "^$(quote_bre $patch)\$" \
+	   && grep -q "^$(quote_grep_re $patch)\$" \
 		   $QUILT_PC/applied-patches
 	then
 		mv $QUILT_PC/$patch $QUILT_PC/$proper_name \
